# actualizacion-tp2

Istea actualizacion tecnologica trabajo practico 2 "Dockerfile".


Si se quiere modificar la web que se muestra, debemos hacerlo en el directorio /var/www/html de nuestro host.


Clonar el repositorio.

Una vez descargado, igresar a la carpeta raiz del proyecto y ejecutrar los comandos especificados en el fichero 'docker-run'.


Para probar que funcione bien, ingresar desde el navegador a "actualizacion.istea:8888" (Si se cambia el puerto en el docker run del reverse proxy, cambiarlo aca tambien).

